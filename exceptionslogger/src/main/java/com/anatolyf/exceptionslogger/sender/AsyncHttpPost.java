package com.anatolyf.exceptionslogger.sender;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;

import com.anatolyf.exceptionslogger.Constants;

/**
 * This class is used to perform asynchroniously HTTP calls.
 */
public class AsyncHttpPost extends AsyncTask<String, String, String> {
    private HashMap<String, String> data = null;// post data
    private String urlStr = null;
    private Callback callback = null;

    /**
     * constructor
     */
    public AsyncHttpPost(HashMap<String, String> data, String urlStr, Callback callback) {
        this.data = data;
        this.urlStr = urlStr;
        this.callback = callback;
    }

    /**
     * background
     */
    @Override
    protected String doInBackground(String... params) {
        try {
            HttpRequest req = new HttpRequest(urlStr);
            JSONObject res = req.preparePost().withData(data).sendAndReadJSON();
            if (callback != null) {
                callback.execute();
            }
        }
        catch (Exception e) {
            Log.e(Constants.LOG_TAG, "Sending exceptions to remote server failed: "+e.getMessage(), e);
        }
        return "";
    }

    /**
     * on getting result
     */
    @Override
    protected void onPostExecute(String result) {
        // something...
    }

    interface  Callback {
        void execute();
    }
}
