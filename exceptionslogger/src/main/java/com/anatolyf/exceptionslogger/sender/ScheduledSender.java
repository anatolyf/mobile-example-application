package com.anatolyf.exceptionslogger.sender;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;

import com.anatolyf.exceptionslogger.Constants;
import com.anatolyf.exceptionslogger.db.DBManager;
import com.anatolyf.exceptionslogger.db.DatabaseHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This class is used to send collected exceptions in SQLite android database to rmote server as asynchronous operation
 * with predifed interval of time.
 */

public class ScheduledSender {
    private static int interval = -1;
    private static String url;
    private static final Handler h = new Handler();
    private static Runnable r;
    private static DBManager dbManager;

    private static final String CONFIG_PROP_INTERVAL = "intervl";
    private static final String CONFIG_PROP_URL = "url";

    /**
     * This method initiate the sending scheduler by sending all already collected exceptions immediately
     * and scheduling the next sending session.
     * @param dbManager - DB util
     * @param url - URL of remote server. If it's empty or null, the sending process will not be schduled.
     * @param interval - the interval of time between sending sessions. If it's less or equal to 0, the sending session will not be scheduled
     */
    public static void init(DBManager dbManager, String url, int interval) {
        ScheduledSender.dbManager = dbManager;
        setInterval(interval);
        setUrl(url);
        schedule(ScheduledSender.url,  ScheduledSender.interval);
    }

    /**
     * This method defines the URL of the remote server to send exceptions to.
     * All already collected exceptions and new from now will be sent to this server.
     * @param url - URL of the remote server to send exceptions to.
     */
    public static void setUrl(String url) {
        if (url == null || url.isEmpty()) {
            Log.e(Constants.LOG_TAG, "The provided URL is null or empty, exceptions sending will not be scheduled.");
            return;
        }
        ScheduledSender.url = url;
        dbManager.setConfigProp(CONFIG_PROP_URL, url==null?"":url);
        schedule(ScheduledSender.url,  ScheduledSender.interval);
    }
    /**
     * This method return defined URL of the remote server from DB.
     * @return - URL of the remote server to send exceptions to.
     */
    public static String getUrl() {
        if (ScheduledSender.url == null) {
            ScheduledSender.url = dbManager.getConfigProp(CONFIG_PROP_URL);
        }
        return ScheduledSender.url;
    }

    /**
     * This method defines sending interval in milliseconds.
     * All already collected till now exceptions will be sent to remote server immediately and then the scheduler will be defined with provided interval/
     * @param interval - sending interval in milliseconds.
     */
    public static void setInterval(int interval) {
        if (interval <= 0) {
            Log.e(Constants.LOG_TAG, "The provided interval is less or equal that 0, exceptions sending will not be scheduled.");
            return;
        }
        ScheduledSender.interval = interval;
        dbManager.setConfigProp(CONFIG_PROP_INTERVAL, String.valueOf(interval));
        schedule(ScheduledSender.url,  ScheduledSender.interval);
    }

    /**
     * This method return defined sending interval in milliseconds from DB.
     * @return - sending interval in milliseconds.
     */
    public static int getInterval() {
        if (ScheduledSender.interval == -1) {
            try {
                ScheduledSender.interval = Integer.parseInt(dbManager.getConfigProp(CONFIG_PROP_INTERVAL));
            } catch (Throwable t) {
                Log.e(Constants.LOG_TAG, "Failed to retrieve sending interval from DB", t);
            }
        }
        return ScheduledSender.interval;
    }

    /**
     * This method sends asynchronously all already collected exceptions to the remote server and schdule next sending session according to defined interval.
     */
    private static void schedule(final String url, final int interval) {
        if (url == null || url.isEmpty()) {
            Log.e(Constants.LOG_TAG, "The provided URL is null or empty, exceptions sending will not be scheduled.");
            return;
        }
        if (interval <= 0) {
            Log.e(Constants.LOG_TAG, "The provided interval is less or equal that 0, exceptions sending will not be scheduled.");
            return;
        }
        if (r != null) {
            h.removeCallbacks(r);
        }
        r = new Runnable()
        {
            @Override
            public void run()
            {
                final Map<String, String> records = ScheduledSender.dbManager.getAllExceptions();
                if (records != null && !records.isEmpty()) {
                    List<JSONObject> excs = new LinkedList<>();
                    StringBuilder ids = new StringBuilder();
                    for (Map.Entry<String, String> record : records.entrySet()) {
                        try {
                            excs.add(new JSONObject(record.getValue()));
                            if (ids.length()==0) {
                                ids.append("( ").append(record.getKey());
                            } else {
                                ids.append(" ,").append(record.getKey());
                            }
                            if (excs.size() ==  Constants.MAX_EXCEPTION_PER_SESSION) {
                                ids.append(" )");
                                HashMap<String, String> data = new HashMap<>();
                                data.put("exceptions", new JSONArray(excs).toString());
                                final String inClause = ids.toString();
                                try {
                                    AsyncHttpPost post = new AsyncHttpPost(data, url, new AsyncHttpPost.Callback(){
                                        @Override
                                        public void execute() {
                                            dbManager.delete(DatabaseHelper.TABLE_ECEPTIONS, DatabaseHelper._ID + " in " + inClause, null);
                                        }
                                    });
                                    post.execute();
                                } catch (Throwable t) {
                                    Log.e(Constants.LOG_TAG, "Sending exception to remote server failed: "+t.getMessage(), t);
                                }
                                excs = new LinkedList<>();
                                ids = new StringBuilder();
                            }
                        } catch (JSONException e) {
                            Log.e(Constants.LOG_TAG, "The exception cannot be JSONized and will be skipped from sending to remote server: "+record);
                        }
                    }
                    if (excs.size() > 0) {
                        ids.append(" )");
                        HashMap<String, String> data = new HashMap<>();
                        data.put("exceptions", new JSONArray(excs).toString());
                        final String inClause = ids.toString();
                        try {
                            AsyncHttpPost post = new AsyncHttpPost(data, url, new AsyncHttpPost.Callback(){
                                @Override
                                public void execute() {
                                    dbManager.delete(DatabaseHelper.TABLE_ECEPTIONS, DatabaseHelper._ID + " in " + inClause, null);
                                }
                            });
                            post.execute();
                        } catch (Throwable t) {
                            Log.e(Constants.LOG_TAG, "Sending exception to remote server failed: "+t.getMessage(), t);
                        }
                        excs = new LinkedList<>();
                        ids = new StringBuilder();
                    }
                }
                h.postDelayed(this, interval);
            }
        };
        h.postDelayed(r, 1);
    }
}
