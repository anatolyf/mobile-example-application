package com.anatolyf.exceptionslogger;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.anatolyf.exceptionslogger.db.DBManager;
import com.google.gson.Gson;


import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This class is used to collect all catched and uncatched exceptions including device parameters
 * convert them to JSON and asynchronously store in android SQLite database.
 */

class ExceptionsCollector {

    public enum ExceptionType {
        CATCHED,
        UNCATCHED;
    }

    private DBManager dbManager;

    ExceptionsCollector(Activity activity, DBManager dbManager) {
        this.dbManager = dbManager;
    }

    /**
     * Log exception as "catched" exception with device parameters in predictable order.
     * @param t
     */
    void logException(Throwable t) {
        logException(t, ExceptionType.CATCHED);
    }

    /**
     * Log exception with provided type and device parameters in predictable order.
     * @param t - the exception
     * @param type - exception type. ExceptionsCollector.ExceptionType should be used.
     */
    void logException(Throwable t, ExceptionType type) {
        Gson gson = new Gson();

        Map<String, String> exc_map = new LinkedHashMap<String, String>();
        exc_map.put("time", Constants.DATE_FORMAT.format(new Date()));
        exc_map.put("ex_type", type.toString());
        exc_map.put("message", t.toString());
        exc_map.put("stacktrace", Log.getStackTraceString(t));
        exc_map.put("bootloader", Build.BOOTLOADER);
        exc_map.put("board", Build.BOARD);
        exc_map.put("brand", Build.BRAND);
        exc_map.put("device", Build.DEVICE);
        exc_map.put("display", Build.DISPLAY);
        exc_map.put("fingerprint", Build.FINGERPRINT);
        exc_map.put("hardware", Build.HARDWARE);
        exc_map.put("id", Build.ID);
        exc_map.put("manufactured", Build.MANUFACTURER);
        exc_map.put("model", Build.MODEL);
        exc_map.put("product", Build.PRODUCT);
        exc_map.put("type", Build.TYPE);
        new StoreExceptionInDB().execute(gson.toJson(exc_map));
    }

    private class StoreExceptionInDB extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            dbManager.insertException(params[0]);
            return "Executed.";
        }

        @Override
        protected void onPostExecute(String result) {}

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }
}
