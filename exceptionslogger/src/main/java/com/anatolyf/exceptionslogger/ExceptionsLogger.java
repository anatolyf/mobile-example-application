package com.anatolyf.exceptionslogger;

import android.app.Activity;
import android.util.Log;

import com.anatolyf.exceptionslogger.db.DBManager;
import com.anatolyf.exceptionslogger.sender.ScheduledSender;

/**
 * This class is main interface for external integration of excpetion logger, It used to init the loggin process and to log exceptions themselve.
 */

public class ExceptionsLogger {

    private static ExceptionsCollector collector = null;
    private static Thread.UncaughtExceptionHandler oldHandler;

    /**
     * This method initiate only collecting of uncatched exceptions without sending thmem to remote server.
     * As well, loggin catched exceptions are possible after this nnitialization as well,
     *
     * @param activity - main activity
     */
    public static void init(Activity activity) {
        init(activity, null, -1);
    }

    /**
     * This method initiate collecting of uncatched exceptions including sending them to remote server.
     * If URL parameter is null or empty or sending interval is less or equal than 0, the sending exceptions will not be performed, only collection.
     * All collected till now exceptions will be sent to remote server immediately and then the scheduler will be defined with provided interval/
     * @param activity - main activity
     * @param url - URL of the remote server to send exceptions to.
     * @param interval - Sending interval in milliseconds.
     */
    public static void init(Activity activity, String url, int interval) {
        DBManager dbManager = new DBManager(activity);
        dbManager = dbManager.open();
        collector = new ExceptionsCollector(activity, dbManager);
        oldHandler = Thread.getDefaultUncaughtExceptionHandler();
        ScheduledSender.init(dbManager, url, interval);
        Thread.setDefaultUncaughtExceptionHandler(
                new Thread.UncaughtExceptionHandler() {
                    @Override
                    public void uncaughtException(
                            Thread thread,
                            Throwable throwable
                    ) {
                        collector.logException(throwable, ExceptionsCollector.ExceptionType.UNCATCHED);
                        Log.e(Constants.LOG_TAG, throwable.getMessage(), throwable);
                        if (oldHandler != null)
                            oldHandler.uncaughtException(
                                    thread,
                                    throwable
                            );
                        else
                            System.exit(2);
                    }
                });
    }

    /**
     * This method defines the URL of the remote server to send exceptions to.
     * All already collected exceptions and new from now will be sent to this server.
     * @param url - URL of the remote server to send exceptions to.
     */
    public static void setUrl(String url) {
        ScheduledSender.setUrl(url);
    }

    /**
     * This method return defined URL of the remote server to send exceptions to.
     * @return - URL of the remote server to send exceptions to.
     */
    public static String getUrl() {
        return ScheduledSender.getUrl();
    }

    /**
     * This method defines sending interval in milliseconds.
     * All already collected till now exceptions will be sent to remote server immediately and then the scheduler will be defined with provided interval/
     * @param interval - sending interval in milliseconds.
     */
    public static void setInterval(int interval) {
        ScheduledSender.setInterval(interval);
    }

    /**
     * This method return defined sending interval in milliseconds.
     * @return - sending interval in milliseconds.
     */
    public static int getInterval() {
        return ScheduledSender.getInterval();
    }

    /**
     * This method log exception and a lot of device parameters to be sent to remote server. This exception will be sent
     * to remote server within next sending session according to defined sending interval.
     * @param throwable - the exception
     * @throws Exception - in case the exceptions logger was not initiated yet.
     */
    public static void logException(Throwable throwable) throws Exception {
        if (collector == null) {
            throw new Exception("Exceptions logger is not initialized yet. Run ExceptionsLogger.init first ");
        }
        collector.logException(throwable);
    }
}
