package com.anatolyf.exceptionslogger;

import java.text.SimpleDateFormat;

/**
 * Created by anatoly.f on 5/17/2017.
 */

public class Constants {

    public static final String LOG_TAG = "EL";
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd  HH:mm:ss z");

    public static int MAX_EXCEPTION_PER_SESSION = 5;
}
