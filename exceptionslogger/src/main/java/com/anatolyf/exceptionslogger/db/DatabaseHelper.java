package com.anatolyf.exceptionslogger.db;

/**
 * This class maintain DB. During installation of the framework it create DB with relevant tables.
 * During update of the framework, deletes all the tables and recreate them.
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Table Name
    public static final String TABLE_ECEPTIONS = "EXCEPTIONS";
    public static final String TABLE_CONFIG = "CONFIG";

    // Table columns
    public static final String _ID = "_id";
    public static final String DESC = "exception";
    public static final String NAME = "name";
    public static final String VALUE = "value";


    // Database Information
    static final String DB_NAME = "LOG_EXCEPTIONS.DB";

    // database version
    static final int DB_VERSION = 1;

    // Creating table query
    private static final String CREATE_EXCEPTIONS_TABLE = "create table " + TABLE_ECEPTIONS + " (" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + DESC + " TEXT NOT NULL);";
    private static final String CREATE_CONFIG_TABLE = "create table " + TABLE_CONFIG + " (" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + NAME + " TEXT NOT NULL, " + VALUE + " TEXT NOT NULL);";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_EXCEPTIONS_TABLE);
        db.execSQL(CREATE_CONFIG_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ECEPTIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONFIG);
        onCreate(db);
    }
}
