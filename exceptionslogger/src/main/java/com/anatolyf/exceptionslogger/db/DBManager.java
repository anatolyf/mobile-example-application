package com.anatolyf.exceptionslogger.db;

/**
 * This class is used to perform DB operations
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class DBManager {

    private DatabaseHelper dbHelper;

    private Context context;

    private SQLiteDatabase database;

    public DBManager(Context c) {
        context = c;
    }

    public DBManager open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    /**
     * Inserts exception to DB
     * @param desc - the description of the exception
     */
    public void insertException(String desc) {
        ContentValues contentValue = new ContentValues();
        contentValue.put(DatabaseHelper.DESC, desc);
        database.insert(DatabaseHelper.TABLE_ECEPTIONS, null, contentValue);
    }

    /**
     * Returns all collected exceptions in DB and their IDs to be delted after sending to remote server.
     *
     * @return
     */
    public Map<String, String> getAllExceptions() {
        String selectQuery = "SELECT  * FROM " + DatabaseHelper.TABLE_ECEPTIONS;
        Cursor cursor = database.rawQuery(selectQuery, null);
        Map<String, String> retVal = new LinkedHashMap<>();
        if (cursor != null && cursor.moveToFirst()) {
            int descColumnIx = cursor.getColumnIndex(DatabaseHelper.DESC);
            int idColumnIx = cursor.getColumnIndex(DatabaseHelper._ID);
            do {
                retVal.put(String.valueOf(cursor.getInt(idColumnIx)), cursor.getString(descColumnIx));
            } while (cursor.moveToNext());
        }
        if (cursor != null) {
            cursor.close();
        }
        return retVal;
    }

    /**
     * Delete record from specific DB table with specific where clause
     * @param table - table name
     * @param whereClause - where clause
     * @param whereArgs - where arguments
     */
    public void delete(String table, String whereClause, String[] whereArgs) {
        database.delete(table, whereClause, null);
    }

    /**
     * Update exception description with specific ID in DB
     * @param _id - the id of the exception
     * @param desc - new description
     * @return - return the number of affected rows, in our case always 1 as ID is unique
     */
    public int updateException(long _id, String desc) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.DESC, desc);
        int i = database.update(DatabaseHelper.TABLE_ECEPTIONS, contentValues, DatabaseHelper._ID + " = " + _id, null);
        return i;
    }

    /**
     * Delete exception with specific ID
     * @param _id - is of the exception
     */
    public void deleteException(long _id) {
        database.delete(DatabaseHelper.TABLE_ECEPTIONS, DatabaseHelper._ID + "=" + _id, null);
    }

    /**
     * Retrieve framework configuration from the DB
     * @param name - name of the property.
     * @return - value of provided property
     */
    public String getConfigProp(String name) {
        String selectQuery = "SELECT  * FROM " + DatabaseHelper.TABLE_CONFIG + " WHERE " + DatabaseHelper.NAME + " = '" + name + "'";
        try {
            Cursor cursor = database.rawQuery(selectQuery, null);
            String retVal = null;
            if (cursor != null && cursor.moveToFirst()) {
                retVal = cursor.getString(cursor.getColumnIndex(DatabaseHelper.VALUE));
            }
            if (cursor != null) {
                cursor.close();
            }
            return retVal;
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return null;
    }

    /**
     * Sets provided framework property with its value
     * @param name - property name
     * @param value - property value
     */
    public void setConfigProp(String name, String value) {
        String oldValue = getConfigProp(name);
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.NAME, name);
        contentValues.put(DatabaseHelper.VALUE, value);
        if (oldValue == null) {
            database.insert(DatabaseHelper.TABLE_CONFIG, null, contentValues);
        } else {
            database.update(DatabaseHelper.TABLE_CONFIG, contentValues, DatabaseHelper.NAME + " = '" + name + "'", null);
        }
    }


}