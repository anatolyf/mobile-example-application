package com.anatolyf.exceptionslogger;

import android.app.Activity;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void testLogCollector1() throws Exception {
        try {
            ExceptionsLogger.logException(new Exception("test exception"));
            assertTrue("exception not initialized was not thrown.", false);
        } catch (NullPointerException e) {
            assertTrue("exception not initialized was not thrown.", false);
        } catch (Exception e) {
            assertTrue(true);
        }
    }
}