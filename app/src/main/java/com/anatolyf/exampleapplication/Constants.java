package com.anatolyf.exampleapplication;

/**
 * Created by anatoly.f on 5/17/2017.
 */

public class Constants {

    public static final String DEFAULT_URL = "http://10.0.0.33:9000/api/exceptions";
    public static final int DEFAULT_INTERVAL = 600;

    public static final String LOG_TAG = "EA";
}
