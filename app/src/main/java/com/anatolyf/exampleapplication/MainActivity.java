package com.anatolyf.exampleapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.anatolyf.exceptionslogger.ExceptionsLogger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class MainActivity extends Activity {




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ExceptionsLogger.init(this);
        String url = ExceptionsLogger.getUrl();
        if (url == null || url.isEmpty()) {
            url = Constants.DEFAULT_URL;
        }
        ExceptionsLogger.setUrl(url);
        ((EditText)findViewById(R.id.editText1)).setText(url);
        int interval = ExceptionsLogger.getInterval();
        if (interval == -1) {
            interval = Constants.DEFAULT_INTERVAL*1000;
        }
        ExceptionsLogger.setInterval(interval);
        ((EditText)findViewById(R.id.editText2)).setText(String.valueOf(interval/1000));
    }

    public void throwUncatched(View view) {
        Toast.makeText(this, "Uncatched exception is thrown.", Toast.LENGTH_LONG).show();
        throw new RuntimeException("This is a crash");
    }

    public void throwCatched(View view) {
        try {
            throw new Exception("test exception");
        } catch (Exception e) {
            try {
                ExceptionsLogger.logException(e);
                Toast.makeText(this, "Catched exception is logged.", Toast.LENGTH_LONG).show();
            } catch (Exception e1) {
                Log.e(Constants.LOG_TAG, e1.getMessage(), e1);
            }
        }
    }

    public void setURL(View view) {
        String url = ((EditText)findViewById(R.id.editText1)).getText().toString();
        String message = null;
        if (url.length()==0) {
            message = "URL cannot be empty";
        }
        if (message == null) {
            URLConnection con = null;
            try {
                URL u = new URL(url);
                con = u.openConnection();
                con.connect();
            } catch (Exception e) {
                message = e.getMessage();
            } finally {
                if (con != null) {
                    ((HttpURLConnection)con).disconnect();
                }
            }
        }
        if (message != null) {
            AlertDialog.Builder messageBox = new AlertDialog.Builder(this);
            messageBox.setTitle("URL");
            messageBox.setMessage(message);
            messageBox.setCancelable(false);
            messageBox.setNeutralButton("OK", null);
            messageBox.show();
            return;
        }
        ExceptionsLogger.setUrl(url);
        Toast.makeText(this, "URL is defined.", Toast.LENGTH_LONG).show();
    }

    public void setPeriod(View view) {
        String intervalStr = ((EditText)findViewById(R.id.editText2)).getText().toString();
        String message = null;
        if (intervalStr.length()==0) {
            message = "Interval cannot be empty";
        }
        int interval = 0;
        if (message == null) {
            try {
                interval = Integer.parseInt(intervalStr);
                interval = interval*1000;
            } catch (Exception e) {
                message = "Interval must be interger";
            }
        }
        if (message != null) {
            AlertDialog.Builder messageBox = new AlertDialog.Builder(this);
            messageBox.setTitle("Interval");
            messageBox.setMessage(message);
            messageBox.setCancelable(false);
            messageBox.setNeutralButton("OK", null);
            messageBox.show();
            return;
        }
        ExceptionsLogger.setInterval(interval);
        Toast.makeText(this, "Interval is defined.", Toast.LENGTH_LONG).show();
    }

}
